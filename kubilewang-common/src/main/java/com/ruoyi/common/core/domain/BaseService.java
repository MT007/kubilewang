package com.ruoyi.common.core.domain;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author kubilewang
 * @date 2020年3月24日
 */
public interface BaseService<T> extends IService<T> {
}
